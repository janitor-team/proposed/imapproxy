#!/usr/bin/make -f
%:
	dh  $@

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall
export DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,defs -Wl,--as-needed
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

override_dh_auto_clean:
	dh_auto_clean
	debconf-updatepo

	# Autotools generated files.
	rm -f config.status
	rm -f config.guess
	rm -f config.h.in
	rm -f config.log
	rm -f config.sub
	rm -f configure

	rm -rf debian/imapproxy

override_dh_auto_configure:

	ln -sf /usr/share/misc/config.sub config.sub
	ln -sf /usr/share/misc/config.guess config.guess

	./configure \
		--host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--mandir=\$${prefix}/share/man \
		--infodir=\$${prefix}/share/info \
		--with-openssl=/usr/include/openssl \
		--with-libwrap \
		$(shell dpkg-buildflags --export=configure)

override_dh_auto_build:

	mkdir -p bin
	dh_auto_build

override_dh_auto_install:

	$(MAKE) install prefix=$(CURDIR)/debian/imapproxy/usr
	mv debian/imapproxy/usr/sbin/in.imapproxyd \
		debian/imapproxy/usr/sbin/imapproxyd

	install -d debian/imapproxy/usr/share/imapproxy
	install -m 644 scripts/imapproxy.conf \
		debian/imapproxy/usr/share/imapproxy/imapproxy.conf.default
	install -m 755 debian/prepare-chroot \
		debian/imapproxy/usr/share/imapproxy

	dh_installexamples
	chmod 644 debian/imapproxy/usr/share/doc/imapproxy/examples/imapproxy.conf

override_dh_installinit:
	dh_installinit --no-start -- defaults 98

override_dh_installsystemd:
	dh_installsystemd --no-start

override_dh_compress:
	dh_compress -Ximapproxy.conf
